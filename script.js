/*
Теоретичні питання
1. Опишіть своїми словами як працює метод forEach.
метод forEach  викликає функцію для перебору кожного елемента масиву, але нічого не повертає

2. Як очистити масив?
масив можна очистити методом arr.length = 0;

3. Як можна перевірити, що та чи інша змінна є масивом?
за допомогою методу Array.isArray(arr) можна перевірити чи є данні масивом. Повертає true, якщо
елемент є массивом и false, якщо ні.
*/

function filterBy(array, type) {
    return array.reduce((result, item) => {
        if (typeof item !== type) {
            result.push(item);
        }
        return result
    }, []);
}
console.log(filterBy(['hello', 'world', 23, '23', null],'string'));